import random

usr = 'null'
endGame = False
board = [2]
rand = random.randint(1,7)
for n in range(0, 8):
    if n == rand:
        board.append(2)
    else:
        board.append(0)


def printBoard():
    """ 
    Print board and check for winning tile
    """

    print '======',
    for n in range(0, 9):
        if (n%3 == 0):
            print '\n'
        print board[n],
    print '\n'
    print '======'

    if (1024 in board):
        endGame = True
        print 'Congratulations, you finished the game'

    return True
        
def update(move):
    """ 
    Update board values
    """
    # left
    if move == 'a':
        print "You moved Left"
        for n in range(0,9):
            m = n+1
            if n%3 != 2:
                if (board[n] == board[m]) or (board[n] == 0):
                    board[n] = board[n] + board[m]
                    board[m] = 0

    # right
    elif move == 'd':
        print "You moved Right"
        for n in range(0,9):
            m = n+1
            if n%3 != 2:
                if (board[m] == board[n])  or (board[m] == 0):
                    board[m] = board[m] + board[n]
                    board[n] = 0 

    # down
    elif move == 's':
        print "You moved Down"
        for n in range(0,6):
            m = n+3
            if (board[m] == board[n]) or (board[m] == 0):
                board[m] = board[m] + board[n]
                board[n] = 0 
    
    # up
    elif move == 'w':
        print "You moved Up"
        for n in range(0,6):
            m = n+3
            if (board[m] == board[n]) or (board[n] == 0):
                board[n] = board[n] + board[m]
                board[m] = 0 
    else:
        print 'Impossible'
            
    return True

def checkMoves():
    """ 
    Check moves and add more tiles
    """

    # place a 2 in the first 0 tile found
    if board.count(0) >= 2:
        board[board.index(0)] = 2
    
    elif board.count(0) == 0:
        print 'No moves available' 
        endGame = True

    return True

def main():
    """
    Main Function
    """

    printBoard()
    while not(endGame):
        usr = raw_input('Input move [a] Left [s] Down [d] Right [w] Up: ')
        if (usr == 'a') or (usr == 's') or (usr == 'd') or (usr == 'w'):
            # place user move and recompute board
            update(usr)
            # check and place new tile
            if checkMoves():
                printBoard()
        elif usr != 'null':
            print 'Invalid Input'

    

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.exit(0)
